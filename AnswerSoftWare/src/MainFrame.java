import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MainFrame extends JFrame implements ActionListener {

    private JLabel questionLabel = new JLabel();
    private JPanel choicePanel = new JPanel();
    private JPanel controlPanel = new JPanel();

    private JButton importQuestionBtn = new JButton("导入题库");
    private JButton preBtn = new JButton("上一题");
    private JButton nextBtn = new JButton("下一题");
    private JButton submitBtn = new JButton("提交");

    public MainFrame() {
        setTitle("答题软件");
        setBounds(100, 20, 800, 600);
        questionLabel.setText("1、这是一个测试描述");
        add(questionLabel, BorderLayout.NORTH);
        add(choicePanel, BorderLayout.CENTER);
        add(controlPanel, BorderLayout.SOUTH);
        controlPanel.add(importQuestionBtn);
        controlPanel.add(preBtn);
        controlPanel.add(nextBtn);
        controlPanel.add(submitBtn);

        choicePanel.setLayout(new GridLayout(10, 1));
        JRadioButton first = new JRadioButton("A、选项一");
        JRadioButton second = new JRadioButton("B、选项二");
        JRadioButton third = new JRadioButton("C、选项三");
        JRadioButton forth = new JRadioButton("D、选项四");
        choicePanel.add(first);
        choicePanel.add(second);
        choicePanel.add(third);
        choicePanel.add(forth);

        importQuestionBtn.addActionListener(this);
        preBtn.addActionListener(this);
        nextBtn.addActionListener(this);
        submitBtn.addActionListener(this);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }

    public static void main(String[] args) {
        new MainFrame();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == importQuestionBtn) {
            System.out.println("importQuestionBtn");
        } else if (e.getSource() == preBtn) {
            System.out.println("preBtn");
        } else if (e.getSource() == nextBtn) {
            System.out.println("nextBtn");
        } else if (e.getSource() == submitBtn) {
            System.out.println("submitBtn");
        }
    }
}
