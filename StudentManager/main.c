#include <stdio.h>
#include <stdlib.h>

typedef struct Student
{
    char no[10]; // 学号
    char name[20]; // 姓名
    int age; // 年龄
    float chinese; // 语文分数
    float math; // 数学分数
} Data;

Data datas[1000]; // 所有学生成绩信息，最多可以存放1000条

int size = 0; // 当前存放了多少条学生成绩信息

typedef struct Node
{
    Data data;
    struct Node * next;
} Node;

typedef struct Node * LinkList;

LinkList head;

// |head|->|3|->|2|->|1|

void loadLinkList()
{
    FILE * fp = NULL;
    char filename[100];
    printf("请输入您要加载的文件路径：");
    scanf("%s", filename);
    fp = fopen(filename, "r");

    if (fp == NULL)
    {
        printf("文件打开出错\n");
        return;
    }

    head = (malloc)(sizeof(Node));
    head->next = NULL;

    size = 0;
    while(!feof(fp))
    {
        Data data;
        fscanf(fp, "%s %s %d %f %f", data.no, data.name,
               &data.age, &data.chinese, &data.math);
        LinkList node;
        node = (malloc)(sizeof(Node));
        node->data = data;
        node->next = head->next;
        head->next = node;
        size++;
    }

    fclose(fp);
    printf("数据加载成功\n");
}

void printAllLinkList()
{
    LinkList node = head->next;
    while (node != NULL)
    {
        Data data = node->data;
        printf("%s %s %d %0.1f %0.1f\n", data.no, data.name, data.age, data.chinese, data.math);
        node = node->next;
    }
}

void menu()
{
    printf("========= 欢迎大家使用学生成绩管理系统==========\n");
    printf("\t1、使用线性表来实现\n");
    printf("\t2、使用链表来实现\n");
    printf("\t0、退出\n");
    printf("=================================================\n");
    printf("请输入您的选择>");
}

void subMenu()
{

    printf("\t1、增加学生成绩信息\n");
    printf("\t2、删除学生成绩信息\n");
    printf("\t3、修改学生成绩信息\n");
    printf("\t4、查找学生成绩信息\n");
    printf("\t5、打印所有学生成绩信息\n");
    printf("\t6、对学生成绩信息按不同的功课排序\n");
    printf("\t7、从文件加载学生成绩信息\n");
    printf("\t8、将学生成绩信息保存到文件\n");
    printf("\t0、退出\n");
    printf("=================================================\n");
    printf("请输入您的选择>");
}

int findStudent(char sno[])
{
    int i;
    for (i = 0; i < size; i++)
    {
        Data data = datas[i];
        if (strcmp(sno, data.no) == 0)
        {
            return i;
        }
    }

    return -1;
}

void save()
{
    FILE * fp = NULL;
    char filename[100];
    printf("请输入您要存放的文件路径：");
    scanf("%s", filename);
    fp = fopen(filename, "wb");
    if (fp == NULL)
    {
        printf("打开文件出错\n");
        return;
    }

    int i;
    for (i = 0; i < size - 1; i++)
    {
        fprintf(fp, "%s %s %d %.1f %.1f\n", datas[i].no, datas[i].name,
                datas[i].age, datas[i].chinese, datas[i].math);
    }

    if (size > 0)
    {
        i = size - 1;
        fprintf(fp, "%s %s %d %.1f %.1f", datas[i].no, datas[i].name,
                datas[i].age, datas[i].chinese, datas[i].math);
    }

    fclose(fp);
    printf("导出成功！\n");
}

void saveLinkList()
{
    FILE * fp = NULL;
    char filename[100];
    printf("请输入您要存放的文件路径：");
    scanf("%s", filename);
    fp = fopen(filename, "wb");
    if (fp == NULL)
    {
        printf("打开文件出错\n");
        return;
    }

    LinkList current = head->next;
    while (current != NULL)
    {
        if (current->next != NULL)
        {
            fprintf(fp, "%s %s %d %.1f %.1f\n", current->data.no, current->data.name,
                current->data.age, current->data.chinese, current->data.math);
        }
        else
        {
            fprintf(fp, "%s %s %d %.1f %.1f", current->data.no, current->data.name,
                current->data.age, current->data.chinese, current->data.math);
        }

        current = current->next;
    }

    fclose(fp);
    printf("导出成功！\n");
}

void load()
{
    FILE * fp = NULL;
    char filename[100];
    printf("请输入您要加载的文件路径：");
    scanf("%s", filename);
    fp = fopen(filename, "r");

    if (fp == NULL)
    {
        printf("文件打开出错\n");
        return;
    }

    size = 0;
    while(!feof(fp))
    {
        fscanf(fp, "%s %s %d %f %f", datas[size].no, datas[size].name,
               &datas[size].age, &datas[size].chinese, &datas[size].math);
        size++;
    }

    fclose(fp);
    printf("数据加载成功\n");
}

int partition(int model, int left, int right)
{
    Data tmp = datas[left];

    while (left < right)
    {
        while (left < right && (model == 2 && datas[right].chinese <= tmp.chinese ||
                                model == 1 && datas[right].chinese >= tmp.chinese))
        {
           right--;
        }

        Data leftData = datas[left];
        datas[left] = datas[right];
        datas[right] = leftData;

        while (left < right && (model == 2 && datas[left].chinese >= tmp.chinese ||
                                model == 1 && datas[left].chinese <= tmp.chinese))
        {
            left++;
        }

        leftData = datas[left];
        datas[left] = datas[right];
        datas[right] = leftData;
    }

    return left;
}

// 排序
//  0    1   2    3    4    5    6    7    8
//  60,  10, 90,  30,  70,  40,  80,  50,  20
//                     right
//                     left
// 60， 80， 90， 70， 50， 40， 30， 10， 20
void sort(int model, int left, int right)
{
    if (left < right)
    {
        int pivot = partition(model, left, right);
        sort(model, left, pivot - 1);
        sort(model, pivot + 1, right);
    }
}

void sortLinkList(LinkList slow, int startIndex, int endIndex)
{
    if (startIndex >= endIndex)
    {
        return;
    }

    LinkList base = slow;
    LinkList fast = slow->next;
    while (fast != NULL && startIndex < endIndex)
    {
        while (fast != NULL && fast->data.chinese <= base->data.chinese)
        {
            fast = fast->next;
        }

        if (fast != NULL && fast->data.chinese > base->data.chinese)
        {
            slow = slow->next;
            Data tmp = slow->data;
            slow->data = fast->data;
            fast->data = tmp;
            startIndex++;
        }
    }

    Data tmp = base->data;
    base->data = slow->data;
    slow->data = tmp;

    if (base != NULL)
    {
        sortLinkList(base, 0, startIndex - 1);
    }

    if (slow->next != NULL)
    {
        sortLinkList(slow->next, startIndex + 1, endIndex);
    }
}

int main()
{
    int mainCmd, subCmd;
    do
    {
        menu();
        scanf("%d", &mainCmd);
        fflush(stdin);
        if (mainCmd == 1)
        {
            do
            {
                printf("========= 欢迎大家使用线性表实现的学生成绩管理系统==========\n");
                subMenu();
                scanf("%d", &subCmd);
                fflush(stdin);
                if (subCmd == 1)
                {
                    printf("增加一条学生成绩信息\n");
                    Data data;
                    printf("请输入学号:");
                    scanf("%s", data.no);
                    int stuIndex = findStudent(data.no);
                    if (stuIndex != -1)
                    {
                        printf("您要添加的学生信息已经存在\n");
                        continue;
                    }

                    printf("请输入姓名:");
                    scanf("%s", data.name);
                    printf("请输入年龄:");
                    scanf("%d", &data.age);
                    printf("请输入语文分数:");
                    scanf("%f", &data.chinese);
                    printf("请输入数学分数:");
                    scanf("%f", &data.math);
                    datas[size] = data;
                    size++;
                }
                else if (subCmd == 2)
                {
                    char tmp[10];
                    printf("请输入您要删除的学生对应的学号：");
                    scanf("%s", tmp);
                    int stuIndex = findStudent(tmp);
                    if (stuIndex == - 1)
                    {
                        printf("您要删除的学生不存在\n");
                    }
                    else
                    {
                        int i;
                        for (i = stuIndex; i < size - 1; i++)
                        {
                            datas[i] = datas[i + 1];
                        }

                        // datas[size - 1] = NULL;
                        size--;
                    }
                }
                else if (subCmd == 3)
                {
                    char tmp[10];
                    printf("请输入您要修改的学生对应的学号：");
                    scanf("%s", tmp);
                    int stuIndex = findStudent(tmp);
                    if (stuIndex == - 1)
                    {
                        printf("您要修改的学生不存在\n");
                    }
                    else
                    {
                        printf("请输入姓名:");
                        scanf("%s", datas[stuIndex].name);
                        printf("请输入年龄:");
                        scanf("%d", &datas[stuIndex].age);
                        printf("请输入语文分数:");
                        scanf("%f", &datas[stuIndex].chinese);
                        printf("请输入数学分数:");
                        scanf("%f", &datas[stuIndex].math);
                    }
                }
                else if (subCmd == 4)
                {
                    char tmp[10];
                    printf("请输入您要查找的学生对应的学号：");
                    scanf("%s", tmp);
                    int stuIndex = findStudent(tmp);
                    if (stuIndex == - 1)
                    {
                        printf("您要查找的学生不存在\n");
                    }
                    else
                    {
                        Data data = datas[stuIndex];
                        printf("-------------------------------------------------\n");
                        printf("%-10s%-10s%-10s%-10s%-10s\n", "学号", "姓名", "年龄", "语文", "数学");
                        printf("-------------------------------------------------\n");
                        printf("%-10s%-10s%-10d%-10.1f%-10.1f\n", data.no, data.name, data.age, data.chinese, data.math);

                    }
                }
                else if (subCmd == 5)
                {
                    printf("-------------------------------------------------\n");
                    printf("%-10s%-10s%-10s%-10s%-10s\n", "学号", "姓名", "年龄", "语文", "数学");
                    printf("-------------------------------------------------\n");
                    int i;
                    for (i = 0; i < size; i++)
                    {
                        Data data = datas[i];
                        printf("%-10s%-10s%-10d%-10.1f%-10.1f\n", data.no, data.name, data.age, data.chinese, data.math);
                    }
                    printf("-------------------------------------------------\n");
                }
                else if (subCmd == 6)
                {
                    printf("请选择1、升序；2、降序");
                    int model;
                    scanf("%d", &model);
                    // 排序
                    //  0    1   2    3    4    5    6    7    8
                    //                     j
                    //                     i
                    // 60， 80， 90， 70， 50， 40， 30， 10， 20
                    sort(model, 0, size - 1);

                }
                else if (subCmd == 7)
                {
                    load();
                }
                else if (subCmd == 8)
                {
                    save();
                }
                else
                {
                    break;
                }
            }
            while(subCmd != 0);
            printf("退出使用线性表实现的学生成绩管理系统\n");
        }
        else if (mainCmd == 2)
        {
            do
            {
                printf("========= 欢迎大家使用链表实现的学生成绩管理系统==========\n");
                subMenu();
                scanf("%d", &subCmd);
                fflush(stdin);
                if (subCmd == 1)
                {
                    printf("增加一条学生成绩信息\n");
                    Data data;
                    printf("请输入学号:");
                    scanf("%s", data.no);
                    printf("请输入姓名:");
                    scanf("%s", data.name);
                    printf("请输入年龄:");
                    scanf("%d", &data.age);
                    printf("请输入语文分数:");
                    scanf("%f", &data.chinese);
                    printf("请输入数学分数:");
                    scanf("%f", &data.math);

                    if (head == NULL)
                    {
                        head = (malloc)(sizeof(Node));
                        head->next = NULL;
                    }

                    LinkList node;
                    node = (malloc)(sizeof(Node));
                    node->data = data;
                    node->next = head->next;
                    head->next = node;
                    size++;
                }
                else if (subCmd == 2)
                {
                    printf("链表删除\n");
                    printf("请输入你需要删除的学号：");
                    char no[10];
                    scanf("%s", no);
                    // head->node1->node2->node3

                    LinkList pre = head;
                    while (pre->next !=NULL && strcmp(pre->next->data.no, no) != 0)
                    {
                       pre = pre->next;
                    }

                    if (pre->next == NULL)
                    {
                        printf("你需要删除的学生不存在\n");
                    }
                    else
                    {
                        pre->next = pre->next->next;
                        printf("删除成功\n");
                    }

                }
                else if (subCmd == 3)
                { // 修改

                    printf("请输入你需要修改的学号：");
                    char no[10];
                    scanf("%s", no);
                    // head->node1->node2->node3

                    LinkList current = head->next;
                    while (current !=NULL && strcmp(current->data.no, no) != 0)
                    {
                       current = current->next;
                    }

                    if (current != NULL)
                    {
                        printf("请输入姓名:");
                        scanf("%s", current->data.name);
                        printf("请输入年龄:");
                        scanf("%d", &current->data.age);
                        printf("请输入语文分数:");
                        scanf("%f", &current->data.chinese);
                        printf("请输入数学分数:");
                        scanf("%f", &current->data.math);
                    }
                    else
                    {
                       printf("你需要修改的学生不存在\n");
                    }

                }
                else if (subCmd == 4)
                { // 查找
                    printf("请输入你需要查找的学号：");
                    char no[10];
                    scanf("%s", no);
                    // head->node1->node2->node3

                    LinkList current = head->next;
                    while (current !=NULL && strcmp(current->data.no, no) != 0)
                    {
                       current = current->next;
                    }

                    if (current != NULL)
                    {
                        Data data = current->data;
                        printf("%s %s %d %0.1f %0.1f\n", data.no, data.name, data.age, data.chinese,
                               data.math);
                    }
                    else
                    {
                       printf("你需要查找的学生不存在\n");
                    }

                }
                else if (subCmd == 5)
                {
                    printAllLinkList();
                }
                else if (subCmd == 6)
                {
                    //                                        j
                    //                         i
                    // head ->50->10->90->30->70->40->80->60->20
                    // head ->50->90->10->30->70->40->80->60->20
                    // head ->50->90->70->30->10->40->80->60->20
                    // head ->50->90->70->80->10->40->30->60->20
                    // head ->50->90->70->80->60->40->30->10->20
                    // head ->60->90->70->80->50->40->30->10->20
                    sortLinkList(head->next, 0, size - 1);
                }
                else if (subCmd == 7)
                {
                    loadLinkList();
                }
                else if(subCmd == 8)
                {
                    saveLinkList();
                }
                else
                {
                    break;
                }
            }
            while(subCmd != 0);
            printf("退出使用链表实现的学生成绩管理系统\n");
        }
        else
        {
            break;
        }
    }
    while(mainCmd != 0);
    printf("退出学生成绩管理系统\n");

    return 0;
}
